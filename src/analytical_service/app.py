from fastapi import FastAPI

from . import reports

app = FastAPI()

reports.initialize_app(app)
