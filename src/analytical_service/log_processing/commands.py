from .services import LogParser
from .services import UserActionProcessor
from .services import UserActionParser

from ..domain.services import CartService
from ..domain.services import CartProductService
from ..domain.services import CategoryService
from ..domain.services import ProductService
from ..domain.services import TransactionService
from ..domain.services import UserActionService
from ..domain.services import UserService
from ..database import Session


def parsing_file(file_path: str) -> None:
    cart_product_service = CartProductService(Session())
    cart_service = CartService(Session(), cart_product_service)
    category_service = CategoryService(Session())
    product_service = ProductService(
        category_service=category_service,
        session=Session()
    )
    user_action_service = UserActionService(
        category_service=category_service,
        session=Session(),
        user_service=UserService(Session(), UserActionService(Session()))
    )
    user_service = UserService(
        user_action_service=user_action_service,
        session=Session()
    )
    transaction_service = TransactionService(
        cart_service=cart_service,
        user_service=user_service,
        session=Session(),
    )

    log_parser = LogParser()
    user_action_parser = UserActionParser()
    user_action_processor = UserActionProcessor(
        cart_service=cart_service,
        category_service=category_service,
        product_service=product_service,
        user_action_service=user_action_service,
        transaction_service=transaction_service,
        user_service=user_service
    )

    with open(file_path, 'r') as log_file:
        log_entries = log_parser.parse_lines(log_file)
        user_actions = user_action_parser.parse_actions(log_entries)
        user_action_processor.process_actions(user_actions)
