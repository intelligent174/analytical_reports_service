import geocoder

from datetime import datetime
from functools import cache
from typing import Iterable
from typing import Type
from urllib.parse import parse_qsl
from urllib.parse import urlsplit

from ..domain.services import CategoryService
from ..domain.services import CartService
from ..domain.services import ProductService
from ..domain.services import TransactionService
from ..domain.services import UserActionService
from ..domain.services import UserService
from .dto import AddProductToCartAction
from .dto import LogEntry
from .dto import LogEntryAction
from .dto import PayAction
from .dto import ReceivingPaymentNotificationAction
from .dto import UserAction
from .dto import VisitCategoryAction
from .dto import VisitHomePageAction
from .dto import VisitProductAction
from .enums import UserActionType


@cache
def get_country_by_ip(ip: str) -> str:
    return geocoder.ip(ip).country


class LogParser:
    FIELD_DATE = 2
    FIELD_TIME = 3
    FIELD_IP = 6
    FIELD_URL = 7

    def parse_lines(self, lines: Iterable[str]) -> Iterable[LogEntry]:
        for line in lines:
            yield self.parse_line(line)

    def parse_line(self, line: str) -> LogEntry:
        parts = line.split()

        date = parts[self.FIELD_DATE]
        time = parts[self.FIELD_TIME]
        ip = parts[self.FIELD_IP]
        url = parts[self.FIELD_URL]

        date_time = self._parse_date_time(date, time)
        action = self._parse_action(url)

        return LogEntry(
            date_time=date_time,
            ip=ip,
            action=action,
        )

    def _parse_date_time(self, date: str, time: str) -> datetime:
        return datetime.fromisoformat(f'{date} {time}')

    def _parse_action(self, url: str) -> LogEntryAction:
        parts = urlsplit(url)
        path = parts.path
        args = dict(parse_qsl(parts.query))
        return LogEntryAction(
            path=path,
            args=args,
        )


class UserActionParser:
    def parse_actions(self, entries: Iterable[LogEntry]) -> Iterable[UserAction]:
        for entry in entries:
            yield self.parse_entry(entry)

    def parse_entry(self, entry: LogEntry) -> UserAction:
        path_parts = entry.action.path.strip('/').split('/')

        match path_parts:
            case ['cart']:
                return self._create_action(
                    AddProductToCartAction,
                    entry,
                    cart_id=entry.action.args['cart_id'],
                    product_id=entry.action.args['goods_id'],
                    amount=entry.action.args['amount'],
                )
            case ['pay']:
                return self._create_action(
                    PayAction,
                    entry,
                    user_id=entry.action.args['user_id'],
                    cart_id=entry.action.args['cart_id'],
                )
            case [category_name, product_name]:
                return self._create_action(
                    VisitProductAction,
                    entry,
                    category_name=category_name,
                    product_name=product_name,
                )
            case [success_pay] if success_pay.startswith('success_pay'):
                payment_number = success_pay.rsplit('_', 1)[1]
                return self._create_action(
                    ReceivingPaymentNotificationAction,
                    entry,
                    success_pay=payment_number,
                )

            case [home_page] if not home_page:
                return self._create_action(
                    VisitHomePageAction,
                    entry,
                    url='/'
                )

            case [category_name]:
                return self._create_action(
                    VisitCategoryAction,
                    entry,
                    category_name=category_name,
                )

    def _create_action(self, action_type: Type[UserAction], entry: LogEntry, **kwargs) -> UserAction:
        return action_type(
            date_time=entry.date_time,
            ip=entry.ip,
            country=get_country_by_ip(entry.ip),
            **kwargs,
        )


class UserActionProcessor:
    def __init__(
            self,
            category_service: CategoryService,
            product_service: ProductService,
            cart_service: CartService,
            user_action_service: UserActionService,
            transaction_service: TransactionService,
            user_service: UserService
    ):
        self._category_service = category_service
        self._product_service = product_service
        self._cart_service = cart_service
        self._user_action_service = user_action_service
        self._transaction_service = transaction_service
        self._user_service = user_service
        self._action_context = {}

    def process_actions(self, user_actions: Iterable[UserAction]) -> None:
        for action in user_actions:
            self.create_user_action(action)

    def create_user_action(self, action: UserAction) -> None:
        self._action_context.setdefault(action.ip)

        match action.type:
            case UserActionType.VISIT_CATEGORY:
                self._category_service.create_category(name=action.category_name)
                self._user_action_service.create_user_action(action)

            case UserActionType.VISIT_PRODUCT:
                self._action_context[action.ip] = {
                    'product_name': action.product_name,
                    'category_name': action.category_name
                }

                self._user_action_service.create_user_action(action)

            case UserActionType.ADD_PRODUCT_TO_CART:
                user_action = self._action_context.get(action.ip)
                user_action['product_id'] = action.product_id
                user_action['cart_id'] = action.cart_id

                self._product_service.create_product(
                    product_id=action.product_id,
                    product_name=user_action['product_name'],
                    category_name=user_action['category_name']
                )

                self._cart_service.create_cart(
                    cart_id=action.cart_id,
                    product_id=action.product_id,
                    amount=action.amount,
                    date_time=action.date_time
                )

                self._user_action_service.create_user_action(action)

            case UserActionType.VISIT_HOMEPAGE:
                self._user_action_service.create_user_action(action)

            case UserActionType.PAY:
                user_action = self._action_context.get(action.ip)
                user_action['cart_id'] = action.cart_id
                user_action['user_id'] = action.user_id
                user_action['date_time'] = action.date_time

                self._user_action_service.create_user_action(action)

                self._user_service.create_user(
                    ip=action.ip,
                    user_id=action.user_id,
                    country=action.country
                )

            case UserActionType.RECEIVING_PAYMENT_NOTIFICATION:
                user_action = self._action_context.get(action.ip)

                self._transaction_service.create_transaction(
                    user_id=user_action['user_id'],
                    cart_id=user_action['cart_id'],
                    date_time=user_action['date_time'],
                    transaction_id=action.success_pay
                )

                self._user_action_service.create_user_action(action)
