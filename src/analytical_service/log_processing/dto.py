from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
from typing import Any

from .enums import UserActionType


@dataclass
class LogEntryAction:
    path: str
    args: dict[str, Any]


@dataclass
class LogEntry:
    date_time: datetime
    ip: str
    action: LogEntryAction


@dataclass
class UserAction:
    type: UserActionType = field(init=False)
    date_time: datetime
    ip: str
    country: str


@dataclass
class AddProductToCartAction(UserAction):
    cart_id: int
    product_id: int
    amount: int

    def __post_init__(self):
        self.type = UserActionType.ADD_PRODUCT_TO_CART


@dataclass
class PayAction(UserAction):
    user_id: int
    cart_id: int

    def __post_init__(self):
        self.type = UserActionType.PAY


@dataclass
class ReceivingPaymentNotificationAction(UserAction):
    success_pay: int

    def __post_init__(self):
        self.type = UserActionType.RECEIVING_PAYMENT_NOTIFICATION


@dataclass
class VisitCategoryAction(UserAction):
    category_name: str

    def __post_init__(self):
        self.type = UserActionType.VISIT_CATEGORY


@dataclass
class VisitHomePageAction(UserAction):
    url: str

    def __post_init__(self):
        self.type = UserActionType.VISIT_HOMEPAGE


@dataclass
class VisitProductAction(UserAction):
    category_name: str
    product_name: str

    def __post_init__(self):
        self.type = UserActionType.VISIT_PRODUCT
