from enum import Enum


class UserActionType(Enum):
    VISIT_CATEGORY = 1
    VISIT_PRODUCT = 2
    ADD_PRODUCT_TO_CART = 3
    PAY = 4
    VISIT_HOMEPAGE = 5
    RECEIVING_PAYMENT_NOTIFICATION = 6
