from ..exceptions import ClientError


class CountryMostVisitsWebsiteNotFoundError(ClientError):
    pass


class CountryWithMostCategoryViewsNotFoundError(ClientError):
    pass


class FrequentlyPurchasedCategoryNotFoundError(ClientError):
    pass


class NumberRequestsPerHourNotFoundError(ClientError):
    pass


class UnpaidCartNotFoundError(ClientError):
    pass


class UserWithRepeatPurchasesNotFoundError(ClientError):
    pass


class ViewingTimeCategoryNotFoundError(ClientError):
    pass
