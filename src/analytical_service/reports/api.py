from fastapi import APIRouter
from fastapi import HTTPException
from fastapi import Depends
from fastapi import status

from .exceptions import CountryMostVisitsWebsiteNotFoundError
from .exceptions import CountryWithMostCategoryViewsNotFoundError
from .exceptions import FrequentlyPurchasedCategoryNotFoundError
from .exceptions import NumberRequestsPerHourNotFoundError
from .exceptions import UnpaidCartNotFoundError
from .exceptions import UserWithRepeatPurchasesNotFoundError
from .exceptions import ViewingTimeCategoryNotFoundError
from .schemas import CountryMostVisitsWebsite
from .schemas import CountryWithMostCategoryViews
from .schemas import FrequentlyPurchasedCategory
from .schemas import NumberRequestsPerHour
from .schemas import UnpaidCart
from .schemas import UserWithRepeatPurchases
from .schemas import ViewingTimeCategory
from .services.country_most_visits_website_report import CountryMostVisitsWebsiteReport
from .services.country_with_most_category_views_report import CountryWithMostCategoryViewsReport
from .services.frequently_purchased_category_report import FrequentlyPurchasedCategoryReport
from .services.number_requests_per_hour_report import NumberRequestsPerHourReport
from .services.unpaid_cart_report import UnpaidCartReport
from .services.user_with_repeat_purchases_report import UserWithRepeatPurchasesReport
from .services.viewing_time_category import ViewingTimeCategoryReport

router = APIRouter()


@router.get('/website_traffic/countries', response_model=CountryMostVisitsWebsite)
def get_country_with_most_website_visits(
        report_service: CountryMostVisitsWebsiteReport = Depends()
):
    try:
        return report_service.get_report()
    except CountryMostVisitsWebsiteNotFoundError:
        raise HTTPException(status.HTTP_404_NOT_FOUND) from None


@router.get('/category/country', response_model=CountryWithMostCategoryViews)
def get_country_with_most_category_views(
        report_service: CountryWithMostCategoryViewsReport = Depends()
):
    try:
        return report_service.get_report()
    except CountryWithMostCategoryViewsNotFoundError:
        raise HTTPException(status.HTTP_404_NOT_FOUND) from None


@router.get('/category/category_view_statistics', response_model=ViewingTimeCategory)
def get_most_frequent_viewing_time_of_category(
        report_service: ViewingTimeCategoryReport = Depends()
):
    try:
        return report_service.get_report()
    except ViewingTimeCategoryNotFoundError:
        raise HTTPException(status.HTTP_404_NOT_FOUND) from None


@router.get('/website_traffic/per_hour', response_model=NumberRequestsPerHour)
def get_max_number_requests_per_hour(
        report_service: NumberRequestsPerHourReport = Depends()
):
    try:
        return report_service.get_report()
    except NumberRequestsPerHourNotFoundError:
        raise HTTPException(status.HTTP_404_NOT_FOUND) from None


@router.get('/category/related_products', response_model=FrequentlyPurchasedCategory)
def get_frequently_purchased_category_together_with(
        report_service: FrequentlyPurchasedCategoryReport = Depends()
):
    try:
        return report_service.get_report()
    except FrequentlyPurchasedCategoryNotFoundError:
        raise HTTPException(status.HTTP_404_NOT_FOUND) from None


@router.get('/carts/unpaid', response_model=UnpaidCart)
def get_unpaid_carts(
        report_service: UnpaidCartReport = Depends()
):
    try:
        return report_service.get_report()
    except UnpaidCartNotFoundError:
        raise HTTPException(status.HTTP_404_NOT_FOUND) from None


@router.get('/users', response_model=UserWithRepeatPurchases)
def get_users_who_have_made_repeat_purchases(
        report_service: UserWithRepeatPurchasesReport = Depends()
):
    try:
        return report_service.get_report()
    except UserWithRepeatPurchasesNotFoundError:
        raise HTTPException(status.HTTP_404_NOT_FOUND) from None
