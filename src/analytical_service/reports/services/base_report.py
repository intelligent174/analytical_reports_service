from abc import abstractmethod
from abc import ABC

from fastapi import Depends

from ...database import get_session

from ...database import Session


class BaseReport(ABC):
    def __init__(self, session: Session = Depends(get_session)) -> None:
        self._session = session

    @abstractmethod
    def get_report(self) -> None:
        raise NotImplementedError
