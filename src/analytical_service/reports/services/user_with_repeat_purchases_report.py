from sqlalchemy import func
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from ...domain.models import Transaction
from ...domain.models import User
from ..exceptions import UserWithRepeatPurchasesNotFoundError
from .base_report import BaseReport


class UserWithRepeatPurchasesReport(BaseReport):
    def get_report(self):
        try:
            result = self._session.execute(
                select(func.count('*'))
                .select_from(
                    select(User.id)
                    .join(Transaction, onclause=User.id == Transaction.user_id)
                    .group_by(User.id)
                    .having(func.count('*') > 1)
                    .subquery()
                )
            ).scalar_one()
            return {'number_users': result}
        except NoResultFound:
            raise UserWithRepeatPurchasesNotFoundError from None
