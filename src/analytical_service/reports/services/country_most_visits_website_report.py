from sqlalchemy import desc
from sqlalchemy import func
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from ...domain.models import User
from ..exceptions import CountryMostVisitsWebsiteNotFoundError
from .base_report import BaseReport


class CountryMostVisitsWebsiteReport(BaseReport):
    def get_report(self) -> dict:
        try:
            result = self._session.execute(
                select(User.country, func.count('*').label('number_requests'))
                .group_by('country')
                .order_by(desc('number_requests'))
                .limit(1)
            ).scalar_one()
            return {'country_name': result}
        except NoResultFound:
            raise CountryMostVisitsWebsiteNotFoundError from None
