from sqlalchemy import func
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from ...domain.models import Cart
from ..exceptions import UnpaidCartNotFoundError
from .base_report import BaseReport


class UnpaidCartReport(BaseReport):
    def get_report(self):
        try:
            result = self._session.execute(
                select(Cart, func.count('*').label('number_unpaid_carts'))
                .where(Cart.user_id.is_(None))
            ).fetchone()
            return {'number_carts': result['number_unpaid_carts']}
        except NoResultFound:
            raise UnpaidCartNotFoundError from None
