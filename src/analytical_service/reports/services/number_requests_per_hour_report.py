from sqlalchemy import desc
from sqlalchemy import func
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from ...domain.models import UserAction
from ..exceptions import NumberRequestsPerHourNotFoundError
from .base_report import BaseReport


class NumberRequestsPerHourReport(BaseReport):
    def get_report(self):
        try:
            result = self._session.execute(
                select(func.strftime('%H', UserAction.date_time).label('hours'),
                       func.count('*').label('number_requests'))
                .group_by('hours')
                .order_by(desc('number_requests'))
                .limit(1)
            ).fetchone()
            return {'number_requests': result['number_requests']}
        except NoResultFound:
            raise NumberRequestsPerHourNotFoundError from None
