from sqlalchemy import case
from sqlalchemy import cast
from sqlalchemy import func
from sqlalchemy import select
from sqlalchemy import Integer
from sqlalchemy.exc import NoResultFound

from ...domain.models import UserAction
from ..exceptions import ViewingTimeCategoryNotFoundError
from .base_report import BaseReport


class ViewingTimeCategoryReport(BaseReport):
    def get_report(self):
        try:
            result = self._session.execute(
                select(
                    case(
                        {0: 'night', 1: 'morning', 2: 'afternoon', 3: 'evening'},
                        value=cast(func.extract('hour', UserAction.date_time) / 6, Integer),
                    ).label('interval'),
                )
                .where((UserAction.type == 'VISIT_CATEGORY') & (UserAction.category_name == 'frozen_fish'))
                .group_by('interval')
                .order_by(func.count('*').desc())
                .limit(1)
            ).scalar_one()
            return {'day_part': result}
        except NoResultFound:
            raise ViewingTimeCategoryNotFoundError from None
