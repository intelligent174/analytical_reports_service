from sqlalchemy import distinct
from sqlalchemy import func
from sqlalchemy.exc import NoResultFound

from ...domain.models import Category
from ...domain.models import Product
from ...domain.models import cartproduct
from ..exceptions import FrequentlyPurchasedCategoryNotFoundError
from .base_report import BaseReport


class FrequentlyPurchasedCategoryReport(BaseReport):
    def get_report(self):
        try:
            carts_with_semi_manufactures = self._session.query(
                distinct(cartproduct.c.cart_id), ) \
                .join(Product) \
                .join(Category) \
                .filter(Category.name == 'semi_manufactures') \
                .subquery()

            result = self._session.query(Category.id, Category.name) \
                .select_from(cartproduct) \
                .join(Product) \
                .join(Category) \
                .filter(Category.name.is_not('semi_manufactures')) \
                .filter(cartproduct.c.cart_id.in_(carts_with_semi_manufactures)) \
                .group_by(Category.id, Category.name) \
                .having(func.count(Product.id) > 0) \
                .order_by(func.count(Product.id).desc()) \
                .limit(1).one()
            return {'category': result[1]}
        except NoResultFound:
            raise FrequentlyPurchasedCategoryNotFoundError from None
