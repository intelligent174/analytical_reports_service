from sqlalchemy import desc
from sqlalchemy import func
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from ...domain.models import User
from ...domain.models import UserAction
from ..exceptions import CountryWithMostCategoryViewsNotFoundError
from .base_report import BaseReport


class CountryWithMostCategoryViewsReport(BaseReport):
    def get_report(self):
        try:
            result = self._session.execute(
                select(User.country, func.count('*').label('number_requests'))
                .join_from(User, UserAction)
                .where(UserAction.category_name == 'fresh_fish')
                .group_by('country')
                .order_by(desc('number_requests'))
                .limit(1)
            ).scalar_one()
            return {'country_name': result}
        except NoResultFound:
            raise CountryWithMostCategoryViewsNotFoundError from None
