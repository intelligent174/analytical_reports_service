from pydantic import BaseModel


class CountryMostVisitsWebsite(BaseModel):
    country_name: str


class CountryWithMostCategoryViews(BaseModel):
    country_name: str


class FrequentlyPurchasedCategory(BaseModel):
    category: str


class NumberRequestsPerHour(BaseModel):
    number_requests: int


class UnpaidCart(BaseModel):
    number_carts: int


class UserWithRepeatPurchases(BaseModel):
    number_users: int


class ViewingTimeCategory(BaseModel):
    day_part: str
