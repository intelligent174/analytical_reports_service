from datetime import datetime
from typing import Iterable

from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.exc import NoResultFound

from ..domain.models import Category
from ..domain.models import Cart
from ..domain.models import Product
from ..domain.models import Transaction
from ..domain.models import User
from ..domain.models import UserAction
from ..domain.models import cartproduct
from ..database import Session


class CartService:
    def __init__(self, session: Session, cart_product_service: 'CartProductService'):
        self._session = session
        self._cart_product_service = cart_product_service

    def create_cart(
            self,
            cart_id: int,
            product_id: int,
            amount: int,
            date_time: datetime
    ) -> None:

        cart = Cart(id=cart_id, date_time=date_time)
        self._session.add(cart)
        try:
            self._session.commit()

            self._cart_product_service.create_cart_with_products_and_their_quantity(
                product_id=product_id,
                cart_id=cart.id,
                amount=amount
            )
        except IntegrityError:
            self._cart_product_service.create_cart_with_products_and_their_quantity(
                product_id=product_id,
                cart_id=cart_id,
                amount=amount
            )
            self._session.rollback()

    def get_cart(self, cart_id: int) -> Cart:
        try:
            cart = self._session.execute(
                select(Cart)
                .where(Cart.id == cart_id)
            ).scalar_one()
            return cart
        except NoResultFound:
            pass

    def update_cart(self, cart_id: int, user_id: int) -> None:
        cart = self.get_cart(cart_id)
        cart.user_id = user_id
        self._session.commit()


class CartProductService:
    def __init__(self, session: Session):
        self._session = session

    def create_cart_with_products_and_their_quantity(
            self,
            cart_id: int,
            product_id: int,
            amount: int
    ) -> None:
        query = cartproduct.insert().values(
            product_id=product_id,
            cart_id=cart_id,
            amount=amount,
        )
        self._session.execute(query)
        self._session.commit()


class CategoryService:
    def __init__(self, session: Session):
        self._session = session

    def get_category(self, name: str) -> Category:
        try:
            category = self._session.execute(
                select(Category)
                .where(Category.name == name)
            ).scalar_one()
            return category
        except NoResultFound:
            pass

    def create_category(self, name: str) -> None:
        category = Category(name=name)
        self._session.add(category)
        try:
            self._session.commit()
        except IntegrityError:
            self._session.rollback()


class ProductService:
    def __init__(self, category_service: CategoryService, session: Session):
        self._category_service = category_service
        self._session = session

    def create_product(
            self,
            product_name: str,
            category_name: str,
            product_id: int
    ) -> None:
        category = self._category_service.get_category(category_name)

        product = Product(
            id=product_id,
            name=product_name,
            category_id=category.id
        )
        self._session.add(product)
        try:
            self._session.commit()
        except IntegrityError:
            self._session.rollback()


class TransactionService:
    def __init__(
            self,
            cart_service: CartService,
            user_service: 'UserService',
            session: Session

    ):
        self._cart_service = cart_service
        self._session = session
        self._user_service = user_service

    def create_transaction(
            self,
            transaction_id: int,
            user_id: int,
            cart_id: int,
            date_time: datetime
    ) -> None:
        transaction = Transaction(
            id=transaction_id,
            date_time=date_time,
            cart_id=cart_id,
            user_id=user_id
        )
        self._session.add(transaction)
        try:
            self._session.commit()

            self._cart_service.update_cart(cart_id, user_id)
        except IntegrityError:
            self._session.rollback()


class UserActionService:
    def __init__(
            self,
            session: Session,
            category_service: CategoryService = Depends(),
            user_service: 'UserService' = Depends()
    ):
        self._category_service = category_service
        self._session = session
        self._user_service = user_service

    def get_user_actions(self, ip: str) -> Iterable[UserAction]:
        try:
            user_actions = self._session.execute(
                select(UserAction)
                .where(
                    UserAction.ip_address == ip,
                    UserAction.user_id.is_(None)
                )
            ).scalars().all()
            return user_actions
        except NoResultFound:
            pass

    def create_user_action(self, user_action) -> None:
        user = self._user_service.get_user_by_ip(user_action.ip)

        action = UserAction(
            user_id=user.id if user else None,
            date_time=user_action.date_time,
            type=user_action.type.name,
            ip_address=user_action.ip,
            category_name=getattr(user_action, 'category_name', None),
            product_name=getattr(user_action, 'product_name', None),
            url=getattr(user_action, 'url', None),
            success_pay=getattr(user_action, 'success_pay', None),
            cart_id=getattr(user_action, 'cart_id', None),
            product_id=getattr(user_action, 'product_id', None),
            amount=getattr(user_action, 'amount', None)
        )
        self._session.add(action)
        self._session.commit()

        self.update_user_actions(user, user_action.ip) if user else None

    def update_user_actions(self, user_id: int, ip: str) -> None:
        user_actions = self.get_user_actions(ip)

        for user_action in user_actions:
            user_action.user_id = user_id
        self._session.commit()


class UserService:
    def __init__(self, session: Session, user_action_service: UserActionService = Depends()):
        self._session = session
        self._user_action_service = user_action_service

    def get_user_by_ip(self, ip_address: str) -> User:
        try:
            user = self._session.execute(
                select(User)
                .where(User.ip_address == ip_address)
            ).scalar_one()
            return user
        except NoResultFound:
            pass

    def create_user(
            self,
            ip: str,
            user_id: int,
            country: str
    ) -> None:
        user = User(
            id=user_id,
            ip_address=ip,
            country=country,
        )
        self._session.add(user)
        try:
            self._session.commit()

            self._user_action_service.update_user_actions(user_id, ip)
        except IntegrityError:
            self._session.rollback()
