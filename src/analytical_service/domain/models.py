from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Table
from sqlalchemy.orm import relationship

from ..database import Base


class Category(Base):
    __tablename__ = 'category'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False, unique=True)

    goods = relationship('Product', back_populates='category')


cartproduct = Table(
    'cartproduct',
    Base.metadata,
    Column('id', Integer, primary_key=True),
    Column('product_id', ForeignKey('product.id', ondelete='CASCADE'), nullable=False),
    Column('cart_id', ForeignKey('cart.id', ondelete='CASCADE'), nullable=False),
    Column('amount', Integer, nullable=False)
)


class Product(Base):
    __tablename__ = 'product'

    id = Column(Integer, primary_key=True, autoincrement=False)
    name = Column(String(255), nullable=False)
    category_id = Column(ForeignKey('category.id', ondelete='CASCADE'), nullable=False)

    category = relationship('Category', back_populates='goods')


class Cart(Base):
    __tablename__ = 'cart'

    id = Column(Integer, primary_key=True, autoincrement=False)
    date_time = Column(DateTime, nullable=False)
    user_id = Column(ForeignKey('user.id', ondelete='SET NULL'))

    products = relationship('Product', secondary=cartproduct)
    transaction = relationship('Transaction', back_populates='cart', uselist=False)
    user = relationship('User', back_populates='carts')


class Transaction(Base):
    __tablename__ = 'transaction'

    id = Column(Integer, primary_key=True, autoincrement=False)
    date_time = Column(DateTime, nullable=False)
    cart_id = Column(ForeignKey('cart.id', ondelete='CASCADE'), nullable=False)
    user_id = Column(ForeignKey('user.id', ondelete='CASCADE'), nullable=False)

    cart = relationship('Cart', back_populates='transaction')
    user = relationship('User', back_populates='transactions')


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement=False)
    ip_address = Column(String(255), nullable=False)
    country = Column(String(255), nullable=True)

    transactions = relationship('Transaction', back_populates='user')
    carts = relationship('Cart', back_populates='user')
    user_actions = relationship('UserAction', back_populates='user')


class UserAction(Base):
    __tablename__ = 'useraction'

    id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('user.id', ondelete='SET NULL'))
    date_time = Column(DateTime, nullable=False)
    type = Column(String(255), nullable=False)
    ip_address = Column(String(255), nullable=False)
    category_name = Column(String(255), nullable=True)
    product_name = Column(String(255), nullable=True)
    url = Column(String(255), nullable=True)
    success_pay = Column(Integer, nullable=True)
    cart_id = Column(Integer, nullable=True)
    product_id = Column(Integer, nullable=True)
    amount = Column(Integer, nullable=True)

    user = relationship('User', back_populates='user_actions')
