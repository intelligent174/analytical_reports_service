from pathlib import Path

from dynaconf import Dynaconf

PROJECT_ROOT = Path(__file__).parents[2]
PACKAGE_ROOT = Path(__file__).parents[1]

Settings = Dynaconf

settings = Dynaconf(
    envvar_prefix='FASTAPI_ANALYTICAL_SERVICE',
    settings_files=['settings.toml', '.secrets.toml'],
)


def get_settings() -> Settings:
    return settings
