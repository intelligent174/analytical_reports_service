if __name__ == '__main__':
    import uvicorn

    uvicorn.run('analytical_service.app:app', port=8004)
