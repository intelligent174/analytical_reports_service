import click

from src.analytical_service.log_processing.commands import parsing_file


@click.command()
@click.argument('file_path', type=click.Path(exists=True))
def cli(file_path) -> None:
    """Parsing of server logs"""
    parsing_file(file_path)


if __name__ == '__main__':
    cli()
